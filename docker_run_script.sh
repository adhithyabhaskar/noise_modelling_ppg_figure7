# docker build -t estimate-prf-child:1.0 /home/adhithyabhaskar/Desktop/noise_modelling_ppg_figure7/
BASEDIR="/home/adhithyabhaskar/Desktop/noise_modelling_ppg_figure7/output/containers_100subjv2/*"
OUTPUT="/persistent-data/HCP/output_100subjv2/"

DOCKEROUTPUT="/tmp/output/"
for f in $BASEDIR; do
	echo "$f/"
	sudo docker run -d -ti -v /persistent-data/HCP/:/persistent-data/HCP/ -v $OUTPUT:$DOCKEROUTPUT -v $f/:/tmp/inputdir/ estimate-prf-child:1.0 "/tmp/inputdir/" $DOCKEROUTPUT 2>&1 | tee $f/log.txt
	# break
done


