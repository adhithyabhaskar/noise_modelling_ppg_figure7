clc; clear; close all;

nSubj = 100;

nScans = nSubj*4;
containers_path =  strjoin(["/persistent-data/HCP/output_",nSubj,"subjv2/"],"");
fig_path = 'output/';
files = dir(join([containers_path, '**/rf_values*.mat'],""));
num_mat_found = numel(files);
fprintf("Equality of nScans and number of created folders: %d\n", num_mat_found==nScans)

t_win = [0:0.1:60];
nWin = length(t_win);
CRF_list = zeros(nWin, num_mat_found);
BRF_list = zeros(nWin, num_mat_found);
RRF_list = zeros(nWin, num_mat_found);
r_list = zeros(num_mat_found, 4);

for i = 1:numel(files)
    file_path = fullfile(files(i).folder, files(i).name);
    rf_vals = load(file_path, 'r_PRF_sc','CRF_sc','BRF_sc','RRF_sc');
    CRF_list(:,i)=rf_vals.CRF_sc;
    BRF_list(:,i)=rf_vals.BRF_sc;
    RRF_list(:,i)=rf_vals.RRF_sc;
    r_list(i,:)=rf_vals.r_PRF_sc;
end

CRF_mean = zeros(nWin,1);
BRF_mean = zeros(nWin,1);
RRF_mean = zeros(nWin,1);

CRF_new = zeros(nWin,nScans);
BRF_new = zeros(nWin,nScans);
RRF_new = zeros(nWin,nScans);

for c = 1:num_mat_found
    x = CRF_list(:,c)*r_list(c,2);
    CRF_mean = CRF_mean + x/num_mat_found;
    CRF_new(:,c) = x;
    
    x = BRF_list(:,c)*r_list(c,4);
    BRF_mean = BRF_mean + x/num_mat_found;
    BRF_new(:,c) = x;
    
    x = RRF_list(:,c)*r_list(c,3);
    RRF_mean = RRF_mean + x/num_mat_found;
    RRF_new(:,c) = x;
end

CRF_std = std(CRF_new, 0, 2)/sqrt(num_mat_found);
BRF_std = std(BRF_new, 0, 2)/sqrt(num_mat_found);
RRF_std = std(RRF_new, 0, 2)/sqrt(num_mat_found);

figure('visible','off')
set(gcf, 'Position',[  940   703   555   420]);
ax = plot(t_win,[CRF_mean, BRF_mean, RRF_mean],'linewidth',3), grid on
ax = gca;
ax.GridLineStyle = '--';
ax.Box = 'off';
ax.XAxisLocation = 'origin';

legend('CRF','PARF','RRF');  legend boxoff
xlim([0 50]), ylim([-0.5 0.5])
xlabel('Time (s)')
ylabel('Amplitude (a.u.)')
saveas(gcf, [fig_path,'Figure_7a_',num2str(nSubj),'_v2.png']);
fprintf("Figure saved to %s\n", fig_path)

figure('visible','off')
shadedErrorBar(t_win,CRF_mean,CRF_std,'lineprops','-r') , hold on
shadedErrorBar(t_win,BRF_mean,BRF_std,'lineprops','-k') , hold on
shadedErrorBar(t_win,RRF_mean,RRF_std,'lineprops','-g') , hold on
legend('CRF','BRF','RRF');  legend boxoff
xlim([0 50]), ylim([-0.5 0.5])
% xlabel('Time (s)')
% ylabel('Amplitude (a.u.)')
saveas(gcf, [fig_path,'Figure_7a_shaded',num2str(nSubj),'.png']);
% fprintf("Figure saved to %s\n", fig_path)