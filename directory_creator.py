import os
import csv
from math import ceil

container_root_dir = "/home/adhithyabhaskar/Desktop/noise_modelling_ppg_figure7/output/containers_100subjv2"
subject_list = "/persistent-data/HCP/100_subject_list.txt"
hcp_dir = "/persistent-data/HCP/"
minPeak_range = "[0.45:0.05:0.70]"
outlier_range = "[2:1:8]"

num_subjects = 100
subjects_per_container = 20

tasks = ["REST1_LR", "REST1_RL","REST2_LR", "REST2_RL"]
arr_100 = []
with open(subject_list, 'r') as f:
    arr_100 = f.read().splitlines()

num_containers = ceil(num_subjects/subjects_per_container)
print(f"Root directory {container_root_dir}")
print(f"Creating {num_containers} containers for {num_subjects} subjects with a maximum of {subjects_per_container} subjects in each container")

for cont in range(num_containers):
    container_name = "container" + str(cont) + "/input/"
    cont_path = os.path.join(container_root_dir, container_name)
    os.makedirs(cont_path, exist_ok = True)

    curr_subj_start = cont*subjects_per_container
    curr_subj_end = min(((cont+1)*subjects_per_container), num_subjects)
    curr_subjs = arr_100[curr_subj_start:curr_subj_end]

    subject_path = os.path.join(cont_path, 'subjects.txt')
    with open(subject_path, 'w') as f:
        for subj in curr_subjs:
            f.write(f"{subj}\n")

    tasklist_path = os.path.join(cont_path, 'tasks.txt')
    with open(tasklist_path, 'w') as f:
        for task in tasks:
            f.write(f"{task}\n")

    vars_path = os.path.join(cont_path, 'vars.csv')
    header = ["hcp_dir", "minPeak_all", "filloutliers_ThresholdFactor_hr_all"]
    data = [hcp_dir, minPeak_range, outlier_range]
    with open(vars_path, 'w') as f:
        writer = csv.writer(f, delimiter='|')
        writer.writerow(header)
        writer.writerow(data)

    print(container_name, curr_subjs)
