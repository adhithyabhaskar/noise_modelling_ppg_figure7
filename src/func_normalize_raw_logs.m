function [task_list] = func_normalize_raw_logs(subject, data_dir, fileID)
% Input: Subject number. Should be located in /data/subject/
% Output: Table with:
%   Row 1: Task name
%   Row 2: Raw data from reading log file (trig, cardiac, resp)
%   Row 3: Normalized data of row 2

task_list = {'REST1_LR','REST1_RL','REST2_LR','REST2_RL'};

% String together file name for each task: rfMRI_REST2_LR_Physio_log.txt
for i=1:length(task_list)
    log_list(i)=join(['rfMRI_', task_list(i), '_Physio_log.txt'], "");
end

% Read file into table
log_list_path = fullfile(data_dir, subject, 'MNINonLinear', 'Results', log_list);
for i=1:length(log_list_path)
    try
    task_list{2, i}=readmatrix(log_list_path{i});
    catch ME
        if (strcmp(ME.identifier,'MATLAB:textio:textio:FileNotFound'))
        func_print_and_file(fileID, "The physio log files do not exist for subject %s task %s \n", subject, task_list{1, i})
        end
    end
end

for i=1:length(task_list)
    task_list{3, i}(:,1) = task_list{2, i}(:,1); % not modifying trig
    task_list{3, i}(:,2) = normalize(task_list{2, i}(:,2), 'zscore'); % resp column
    task_list{3, i}(:,3) = normalize(task_list{2, i}(:,3), 'zscore'); % cardiac column
end

end


   
