function [CRF_sc, BRF_sc, RRF_sc, r_PRF_sc, FC, FC_partial, time_10, HR, timeMR, GS, yPred_card, cardiac, yPred_PPG, RV, yPred_resp, yPred] = func_test_param(param1, param2, subject, task, hcp_dir, Fs_10, Ts, Ts_10, volDel, GS, filt_b, filt_a, fileID)
%FUNC_CALCULATE_FMINCON Summary of this function goes here
%   Detailed explanation goes here
        func_create_phys_kscript(subject, hcp_dir, param1, param2, 0.5, fileID);
        [~,~,TR,trig,PPGlocs,HR,~,cardiac,~,resp_10,~,~] = func_load_scan_kscript(subject,task,hcp_dir);

        time = 0:Ts:(length(trig)-1)*Ts;
        time_10 = 0:0.1:time(end);
        ind_BOLD=find(trig==1);
        trig(ind_BOLD(1:volDel))=0;
        ind_BOLD=find(trig==1);
        timeMR=time(trig==1); timeMR = timeMR +TR/2;
        NV = length(timeMR);
        
        GS = filtfilt(filt_b,filt_a,GS) ;
        GS = zscore(GS);
        
        ind_BOLD_10 = zeros(NV,1);
        for i = 1:NV
        t = timeMR(i);        [val, loc] = min(abs(time_10-t));
        ind_BOLD_10(i) = loc;
        end
        
        % Extract cardiac
        indPPGlocs = zeros(size(PPGlocs));
        for i = 1 :length(PPGlocs)
        [val, loc] = min(abs(PPGlocs(i)-time));
        indPPGlocs(i) = loc;
        end
        pks = cardiac(indPPGlocs);
        cardiac = interp1([0,time(indPPGlocs),time(end)],[pks(1),pks',pks(end)],time);
        cardiac = interp1(time, cardiac, time_10);    cardiac = zscore(cardiac(:));

        % Extract RV
        RV = zeros(size(resp_10));
        N = length(resp_10);
        for i = 2:N-1
        ind_1 = i-6*Fs_10;   ind_1 = max(1,ind_1);
        ind_2 = i+6*Fs_10;   ind_2 = min(ind_2, N);
        RV(i) = std(resp_10(ind_1:ind_2));
        end
        RV(1) = RV(2); RV(end) = RV(end-1);    RV = RV(:);
        RF = RV;

        options = optimoptions('fmincon','Display','off','Algorithm','interior-point',...
        'UseParallel',false,'MaxIterations',100,'MaxFunctionEvaluations',3000,'OptimalityTolerance',1e-8);
        %     options = optimoptions('fmincon','Display','iter-detailed','Algorithm','interior-point',...
        %         'UseParallel',false,'MaxIterations',100,'MaxFunctionEvaluations',3000,'OptimalityTolerance',1e-8,'PlotFcn','optimplotfval');    % 'PlotFcn','optimplotfval'
        
        PRF_par = [  3.1    2.5   5.6    0.9    1.9   2.9   12.5    0.5    3.1    2.5   5.6    0.9  ]; PRF_par_0 = PRF_par;
        ub = PRF_par+3;
        lb = PRF_par-3; lb(lb<0)=0;
        
        h_train = @(P) func_PRF_w_ePPG_tmp(P,Ts_10,HR,RF,cardiac, ind_BOLD_10,GS,1,1:NV,1:NV,filt_b,filt_a);
        PRF_par = fmincon(h_train,PRF_par,[],[],[],[],lb,ub,[],options);
        h = @(P) func_PRF_w_ePPG_tmp(P,Ts_10,HR,RF,cardiac,ind_BOLD_10,GS,0,1:NV,1:NV,filt_b,filt_a);
        [obj_function,CRF_sc,RRF_sc,BRF_sc, r_PRF_sc,yPred, yPred_card, yPred_resp, yPred_PPG] = h(PRF_par);
        
        X = [yPred_resp, yPred_card, yPred_PPG,GS];
        FC = corr(X);
        
        FC_partial = partialcorr(X);    
        Part_RV_HR_PPG = partialcorr(yPred_resp,yPred_card,yPred_PPG);
        FC_partial(1,2) = Part_RV_HR_PPG;     FC_partial(2,1) = Part_RV_HR_PPG; 
        Part_RV_PPG_HR = partialcorr(yPred_resp,yPred_PPG,yPred_card);
        FC_partial(1,3) = Part_RV_PPG_HR;     FC_partial(3,1) = Part_RV_PPG_HR; 
        Part_HR_PPG_RV = partialcorr(yPred_card,yPred_PPG,yPred_resp);
        FC_partial(2,3) = Part_HR_PPG_RV;     FC_partial(3,2) = Part_HR_PPG_RV;
end

