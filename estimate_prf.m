function [] = estimate_prf(base_dir, output_dir)
% clear, clc, close all
% addpath(genpath(pwd));

%% from input folder for compiled version
[hcp_dir, minPeak_all,filloutliers_ThresholdFactor_hr_all] = readvars([base_dir,'input/vars.csv'], 'Delimiter','|','Range','2:2');
hcp_dir = hcp_dir{1};
minPeak_all = str2num(minPeak_all{1});
filloutliers_ThresholdFactor_hr_all = str2num(filloutliers_ThresholdFactor_hr_all{1});
% hcp_dir = '../noise_modeling_based_on_ppg/data/'; % location of HCP subject data
% minPeak_all = transpose(0.45:0.05:0.90); % 0.45-0.90
% filloutliers_ThresholdFactor_hr_all = transpose(2:1:15); % 3-15

task_list = readmatrix([base_dir,'input/tasks.txt'], 'OutputType', 'string','Delimiter','\n');
subject_list = readmatrix([base_dir,'input/subjects.txt'], 'OutputType', 'string');

%% paths
% baseDir='../noise_modeling_based_on_ppg/data/';
log_path = [base_dir,'logs/'];
log_file_path = [log_path, 'Estimate_PRF_log_', datestr(clock,'YYYYmmdd_HHMM'), '.txt'];
% load([hcp_dir,'subject_list.mat'])
% subject_list = [subject_list_R3];
% subj_remove = {'199251','114924','100206'};
% for i = 1:length(subj_remove)
%     ind = find(subject_list == subj_remove{i} );  subject_list(ind) = [];
% end

nSubj = length(subject_list);
nTask = length(task_list);
nScans = nSubj*nTask;
% subject_list = ["105014"];
% subject_list = ["103818"; "105014"];
% subject_list = readmatrix("data/available_subjects.txt",OutputType="string");
% task_list = {'REST1_LR','REST1_RL','REST2_LR','REST2_RL'};
% task_list = {'REST2_RL'};

if isfolder(log_path)==0
    mkdir(log_path); end

if isfolder(output_dir)==0
    mkdir(output_dir); end

fileID = fopen(log_file_path,'w');

%%  1:  Load physiological variables (heart rate and respiration) and global signal (GS) from MAT-File

%  Set the following parameters !!

Ts = 1/400; 
% HPF_f = 0.7;  [filt_b,filt_a] = butter(2,HPF_f*2*Ts,'high');

TR = 0.72; 
HPF_f = 0.01;  [filt_b,filt_a] = butter(2,HPF_f*2*TR,'high');

volDel=20;
Ts_10 = 0.1; Fs_10 = 10; t_win= 0:Ts_10:60; N_win = length(t_win);

overwrite = 0; % 0 to not overwrite GS.mat if it already exists for the scan

% Parameter testing section
num_params = 2+4+1+1; % 4 r-values to store, 1 for pass/fail flag, 1 for the optimal parameters
[p,q] = meshgrid(minPeak_all, filloutliers_ThresholdFactor_hr_all);
parameter_space = [p(:) q(:)];
parameter_space_result= [zeros(nScans, length(parameter_space), num_params)];

% Models
% r_all = zeros(nScans,4);
% CRF_all = zeros(N_win,nScans);
% BRF_all = zeros(N_win,nScans);
% RRF_all = zeros(N_win,nScans);
% FC_all = zeros(nScans, 4,4);
% FC_partial_all = zeros(nScans,4,4);
M = 8;
%%  -----------

% Calculate GS.mat for all subjects/scans
func_print_and_file(fileID,"Calculating GS.mat ... \n")
for i=1:nSubj
    for j=1:length(task_list)
        func_print_and_file(fileID, ' ----------------------------------------------- \n')
        func_print_and_file(fileID,'Subject: %s  (%d/%d);   Run: %d/%d \n',subject_list{i},i,nSubj,j,length(task_list));
        func_calc_gs(subject_list{i}, task_list{j}, hcp_dir, overwrite, fileID);
    end
end
func_print_and_file(fileID,"GS creation done \n")

tic
for c = 1:nScans

    s = ceil(c/nTask);        run = c - (s-1)*nTask;
    subject = subject_list{s};         task = task_list{run};
    func_print_and_file(fileID, ' ----------------------------------------------- \n')
    func_print_and_file(fileID, 'Subject: %s     (%d/%d);   Run: %d/%d    \n',subject,s,nSubj,run,length(task_list))

    GS = func_load_gs(subject, task, hcp_dir);

	for param=1:length(parameter_space)
        lastwarn(''); % clear warnings at start of loop

        %% 2: Estimate PRF parameters
        [~, ~, ~, r_PRF_sc, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~]  = func_test_param(parameter_space(param, 1), parameter_space(param, 2), subject, task, hcp_dir, Fs_10, Ts, Ts_10, volDel, GS, filt_b, filt_a, fileID);

        func_print_and_file(fileID, ' ----------------------------------------------- \n')
        
        [~, msgid] = lastwarn;
        if strcmp(msgid,'MATLAB:rankDeficientMatrix')
            func_print_and_file(fileID, 'Warning!: Rank deficient parameters %3.2f and %3.2f  \n', parameter_space(param, 1), parameter_space(param, 2))
            parameter_space_result(c, param,7) = 0; % 0 flag if rank deficient warning
        else
            func_print_and_file(fileID, 'Passed: %3.2f and %3.2f  \n', parameter_space(param, 1), parameter_space(param, 2))
            parameter_space_result(c, param,7) = 1;
        end

        func_print_and_file(fileID, 'Correlation b/w GS and PRF output \n')
        func_print_and_file(fileID, 'CRF (HR): %3.2f  \n',r_PRF_sc(2))
        func_print_and_file(fileID, 'RRF (RF): %3.2f  \n',r_PRF_sc(3))
        func_print_and_file(fileID, 'PPG (BP): %3.2f  \n',r_PRF_sc(4))
        func_print_and_file(fileID, 'CRF & RRF (HR & RF): %3.2f  \n',r_PRF_sc(1))

        parameter_space_result(c, param,1:2) = parameter_space(param,:);
        parameter_space_result(c, param,3:6) = [r_PRF_sc(2), r_PRF_sc(3), r_PRF_sc(4), r_PRF_sc(1)];

        parameter_space_result(c, param,1:2) = parameter_space(param,:);
        parameter_space_result(c, param,3:6) = [r_PRF_sc(2), r_PRF_sc(3), r_PRF_sc(4), r_PRF_sc(1)];
        func_print_and_file(fileID, 'Warning!: %3.2f and %3.2f  \n', parameter_space(param, 1), parameter_space(param, 2))
        parameter_space_result(c, param,7) = 0; % 0 flag if rank deficient warning
    end

    save_path = [output_dir, subject, '_', task, '/']
    fig_path = [save_path, 'plots/']
    func_print_and_file(fileID, 'Saving to: %s \n',save_path);

    if isfolder(fig_path)==0
        mkdir(fig_path); end

    if isfolder(save_path)==0
        mkdir(save_path); end

    % finding parameters that result in the maximum mean r-value
    avg_rvalues = mean(squeeze(parameter_space_result(c,:,3:6)),2);
    [~, max_row] = max(avg_rvalues);
    parameter_space_result(c,max_row,8) = 1;

    optimal1 = parameter_space_result(c,max_row,1);
    optimal2 = parameter_space_result(c,max_row,2);
    optimal_params = [convertCharsToStrings(subject), convertCharsToStrings(task), optimal1, optimal2];
    save([save_path, 'optimal_params.mat'], "optimal_params");

    [CRF_sc, BRF_sc, RRF_sc, r_PRF_sc, FC, FC_partial, time_10, HR, timeMR, GS, yPred_card, cardiac, yPred_PPG, RV, yPred_resp, yPred] = func_test_param(optimal1, optimal2, subject, task, hcp_dir, Fs_10, Ts, Ts_10, volDel, GS, filt_b, filt_a, fileID);

    func_estimate_prf_plot(fig_path, subject, task, CRF_sc, Ts_10, time_10, HR, timeMR, GS, yPred_card, ...
        r_PRF_sc, cardiac, yPred_PPG, RV, yPred_resp, yPred, BRF_sc, RRF_sc, ...
        optimal1, optimal2);
    
    save([save_path, 'workspace.mat']);
    save([save_path, 'rf_values.mat'],'r_PRF_sc','CRF_sc','BRF_sc','RRF_sc','FC','FC_partial')  
    
end
func_print_and_file(fileID, ' ----------------------------------------------- \n')
func_print_and_file(fileID, 'Completed! Time elapsed (minutes): %3.1f  \n', toc/60)


% writematrix(reshape(permute(paramter_space_result, [2 1 3]), nSubj*length(parameter_space), num_params),[save_path, 'parameter_space_result.csv'])
end


%%  ---------------------------------

% plot(r_all)
% 
% mean(r_all)
% 
% CM = squeeze(mean(FC_all))
% imagesc(CM)
% 
% 
% CM_partial = squeeze(mean(FC_partial_all))
% imagesc(CM_partial)
% 
% 
% mean(r_all_wos)
% mean(r_all_ws)
% 
% x1 = r_all_wos(:,1);
% x2 = r_all_ws(:,1);
% 
% x1_sbj = reshape(x1',[4 100]);  x1_sbj = mean(x1_sbj)';
% x2_sbj = reshape(x2',[4 100]);  x2_sbj = mean(x2_sbj)';
% 
% [ttest_p ttest_h] = ttest(x1_sbj,x2_sbj)


%% Figure 7 (a)
%%  --------------------

% nScans = 400;
% 
% CRF_mean = zeros(N_win,1);
% BRF_mean = zeros(N_win,1);
% RRF_mean = zeros(N_win,1);
% for c = 1:nScans
%     x = CRF_all(:,c)*r_all(c,2);
%     CRF_mean = CRF_mean + x/nScans;
%     
%     x = BRF_all(:,c)*r_all(c,4);
%     BRF_mean = BRF_mean + x/nScans;
%     
%     x = RRF_all(:,c)*r_all(c,3);
%     RRF_mean = RRF_mean + x/nScans;
% end
% 
% figure('position', [  940   703   555   420])
% ax = plot(t_win,[CRF_mean, BRF_mean, RRF_mean],'linewidth',3), grid on
% 
% ax = gca;
% ax.GridLineStyle = '--';
% ax.Box = 'off';
% ax.XAxisLocation = 'origin';
% 
% legend('CRF','PARF','RRF');  legend boxoff
% 
% xlim([0 50]), ylim([-0.5 0.5])
% xlabel('Time (s)')
% ylabel('Amplitude (a.u.)')
% 






